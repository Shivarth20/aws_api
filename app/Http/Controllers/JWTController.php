<?php

namespace App\Http\Controllers;

use Auth;
use Validator;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class JWTController extends Controller
{

    /**
     * Register user.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:2|max:100',
            'email' => 'required|string|email|max:100|unique:users',
            'password' => 'required|string|confirmed|min:6',
        ]);

        if($validator->fails()) {
            return response()->json([
                'message' => 'Error in Registration',
                'status' => false,
                'error' => $validator->errors()
            ]);
        }

        $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password)
            ]);

        return response()->json([
            'message' => 'User successfully registered',
            'status' => true,
            'user' => $user
        ], 201);
    }

    /**
     * login user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|string|min:6',
        ]);


        if ($validator->fails()) {
            return response()->json([
                'message' => 'Error in Login',
                'status' => false,
                'error' => $validator->errors()
            ]);
        }

        if (!$token = auth('api')->attempt($validator->validated())) {
            return response()->json(['message' => 'Invalid username or password', 'status' => false]);
        }

        return $this->respondWithToken($token, true, 'Successfully logged In');
    }

    /**
     * Logout user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'User successfully logged out.']);
    }

    /**
     * Refresh token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth('api')->refresh());
    }

    /**
     * Get user profile.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function profile()
    {
        return response()->json(auth('api')->user());
    }

    public function usersList(Request $request) {
        $response = array();
        $inputData = $request->input();
        $limit = isset($inputData['limit']) && $inputData['limit'] ? $inputData['limit'] : 10;
        $offset = isset($inputData['offset']) && $inputData['offset'] ? $inputData['offset'] : 0;
        $details = User::offset($offset)->limit($limit)->get();
        if($details) {
            $response['status'] = true;
            $response['message'] = 'User List';
            $response['data'] = $details;
        } else {
            $response['status'] = true;
            $response['message'] = 'User List';
            $response['data'] = [];
        }
        return $response;
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token, $status = false, $message = '')
    {
        $response = [
            'access_token' => $token,
            'token_type' => 'bearer',
            'user_details' => Auth::user(),
            'expires_in' => auth('api')->factory()->getTTL() * 60
        ];
        if($status) {
            $response['status'] = true;
        }
        if($message) {
            $response['message'] = $message;
        }
        return response()->json($response);
    }
}