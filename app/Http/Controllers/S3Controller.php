<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use Aws\S3\S3Client;  
use Aws\Exception\AwsException;

class S3Controller extends Controller
{

    /**
    * Used to initialize S3Client.
    *
    * @param void
    * @return void
    */
    
    public function __construct()
    {
        $this->s3Client = new S3Client([
            'version' => 'latest',
            'region'  => getenv('AWS_DEFAULT_REGION'),
            'credentials' => [
                'key'    => getenv('AWS_S3_ACCESS_KEY_ID'),
                'secret' => getenv('AWS_S3_SECRET_ACCESS_KEY'),
            ],
        ]);
    }

    /**
    * Used to fetch image present in bucket.
    *
    * @param $request - bucket name
    * @return image_list
    *
    */
    
    public function fetch(Request $request) {
        $inputData = $request->input();
        $result = array();
        if(empty($inputData['bucket_name'])) {
            return $this->jsonResponse(false, 'Please select Bucket name');
        }
        $result['Bucket'] = $inputData['bucket_name'];
        try {
            if(!empty($inputData['directory_name'])) {
                $result['Prefix'] = $inputData['directory_name'] .'/';
            }
            $response = $this->s3Client->listObjects($result);
            $file = array();
            $key = $key1 =  0;
            foreach($response['Contents'] as $fileList) {

                if(substr($fileList['Key'], -1) === '/'){
                    $file['folder'][$key1]['name'] = str_replace('/', '', $fileList['Key']);
                    $key1++;

                } else {
                    $file['file'][$key]['name'] = $fileList['Key'];
                    $file['file'][$key]['url'] = 'https://s3.ap-south-1.amazonaws.com/'.$inputData['bucket_name'].'/'.$fileList['Key'];
                    $file['file'][$key]['size'] = $fileList['Size']/ 1000 . 'Kb';
                    $key++;
                }

            }
            return $this->jsonResponse(true, 'File List', $file);

        } catch (S3Exception $e) {
            return $e->getmessage();
        }
    }

    /**
    * Used to delete file from bucket.
    *
    * @param $request - bucket name, file name
    * @return response
    *
    */

    public function delete(Request $request) {
        $inputData = $request->input();
        $response = array();
        if(empty($inputData['bucket_name'])) {
            return $this->jsonResponse(false, 'Please select Bucket name');
        }

        if(empty($inputData['file'])) {
            return $this->jsonResponse(false, 'Please select file to be deleted');
        }

        $status = true;
        $message = '';
        $result = array();

        try {
            $result = $this->s3Client->deleteObject(array(
                'Bucket' => $inputData['bucket_name'], 
                'Key' => $inputData['file'])
            );
            $message = 'File deleted';
            
        } catch (S3Exception $e) {
            $status = false;
            $message = $e->getMessage();
        }
        return $this->jsonResponse($status, $message , $result);
    }

    /**
    * Used to upload file from bucket.
    *
    * @param $request - bucket name, file name
    * @return response
    *
    */

    public function upload(Request $request) {
        $response = array();
        $file = $request->file('file');
        $inputData = $request->input();
        $name = $file->getClientOriginalName();
        
        if(empty($inputData['bucket_name'])) {
            return $this->jsonResponse(false, 'Please select Bucket name');
        }

        if(empty($name)) {
            return $this->jsonResponse(false, 'Please select file to be uploaded');
        }

        if(!empty($inputData['path'])) {
            $name = $inputData['path']. '/'.$name;
        }

        try {
            $result = $this->s3Client->putObject(array(
                'Bucket'     => $inputData['bucket_name'],
                'Key'        => $name,
                'SourceFile' => $request->file('file'),
            ));

        } catch (S3Exception $e) {
            return $e->getmessage();
        }

        if($result) {
            return $this->jsonResponse(true, 'File uploaded successfully');
        } else {
            return $this->jsonResponse(false, 'Error in uploading image');
        }
    }

    /**
    * list all bucket present in S3.
    *
    * @param $request void
    * @return bucket_list
    *
    */

    public function list() {
        try {
            $buckets = $this->s3Client->listBuckets();
        } catch (S3Exception $e) {
            return $e->getmessage();
        }

        $bucketList = array();

        foreach($buckets['Buckets'] as $list) {
            $bucketList[] = $list['Name'];
        }

        return $this->jsonResponse(true, 'Bucket List', $bucketList);
    }



    /**
    * Used to create new bucket.
    *
    * @param $request - bucket name
    * @return response
    *
    */

    public function createBucket(Request $request) {

        $inputData = $request->input();

        if(empty($inputData['bucket_name'])) {
            return $this->jsonResponse(false, 'Please add Bucket name');
        }

        try {
            $response = $this->s3Client->createBucket([
                'Bucket' => $inputData['bucket_name'], // REQUIRED
            ]);
        } catch (S3Exception $e) {
            return $e->getmessage();
        }

        return $this->jsonResponse(true, 'Bucket Created', $response["@metadata"]);

    }

    /**
    * Create a new directory in S3 bucket.
    *
    * @param $request bucket_name, directory_name
    * @return response
    *
    */

    public function createdirectory(Request $request) {

        $inputData = $request->input();

        if(empty($inputData['bucket_name'])) {
            return $this->jsonResponse(false, 'Please select Bucket to continue');
        }

        if(empty($inputData['directory_name'])) {
            return $this->jsonResponse(false, 'Please select Folder to continue');
        }

        try {
            $response = $this->s3Client->putObject(array( 
                'Bucket' => $inputData['bucket_name'],
                'Key'    => $inputData['directory_name']. '/',
                'Body'   => ""
            ));        } 
            catch (S3Exception $e) {
            return $e->getmessage();
        }

        $bucketList = array();

        return $this->jsonResponse(true, 'Directory Created', $response);
    }


    /**
    * Used to delete bucket.
    *
    * @param $request - bucket name
    * @return response
    *
    */

    public function deleteBucket(Request $request) {

        $inputData = $request->input();

        if(empty($inputData['bucket_name'])) {
            return $this->jsonResponse(false, 'Please select Bucket name to delete');
        }
        try {
            $result = $this->s3Client->deleteBucket([
                'Bucket' => $inputData['bucket_name']
            ]);
            return $this->jsonResponse(true, 'Bucket deleted', $result);

        } catch (S3Exception $e) {
            return $e->getmessage();
        }        
    }

    protected function jsonResponse($status = false, $message = '', $data = array())
    {
        return response()->json([
            'status'  => $status,
            'message' => $message,
            'data'    => $data
        ]);
    }

}
