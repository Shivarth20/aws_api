<?php

namespace App\Http\Controllers;
use Auth;
use Validator;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;


class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }

    public function addUser(Request $request)
    {

        $input_data = $request->input();

       // return $input_data;
       
        $data = User::create([
            'name' => $input_data['name'],
            'password' => $input_data['password'],
            'email' => $input_data['email'],
        ]);

        $response = array();
        if($data) {
            $response['success'] = true;
            $response['message'] = 'User Registered';
            $response['res'] = $data;
        }  else {
            $response['success'] = false;
            $response['message'] = 'Registered Failed';
        }

        return $response;

    }

        //$this->validate($request, [
        //    'name'                => 'required|unique:products',
        //    'parent_category'     => 'required',
        //    'product_description' => 'required',
        //    'product_image'       => 'required',
        //        ]);

        public function authenticateUser(Request $request)
        {
        $input_data = $request->input();

        $users = User::query()
        ->where('email', '=', $input_data['email'])
        ->where('password', '=', $input_data['password'])
        ->get();

        $response = array();
        if(count($users)) {
            $response['success'] = true;
            $response['message'] = 'User Logged In';
            $response['respone'] = $users;
        }  else {
            $response['success'] = false;
            $response['message'] = 'Invalid credential';
            $response['test'] = $users;
        }

        return $response;
    }
}
