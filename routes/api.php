<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\JWTController;
use App\Http\Controllers\S3Controller;




/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function($router) {
    
    // Authorization operation
    Route::post('/logout', [JWTController::class, 'logout']);
    Route::post('/refresh', [JWTController::class, 'refresh']);
    Route::post('/profile', [JWTController::class, 'profile']);
    
    //s3 Bucket opertation
    Route::POST('/lists3bucket', [S3Controller::class, 'list']);
    Route::POST('/createbucket', [S3Controller::class, 'createBucket']);
    Route::POST('/deletebucket', [S3Controller::class, 'deleteBucket']);

    //S3 file operation
    Route::POST('/filedelete', [S3Controller::class, 'delete']);
    Route::POST('/fileupload', [S3Controller::class, 'upload']);
    Route::POST('/filelist', [S3Controller::class, 'fetch']);

    //S3 bucket directory management
    Route::POST('/createdirectory', [S3Controller::class, 'createdirectory']);

    // fetch user details
    Route::post('/users_list', [JWTController::class, 'usersList']);
});

Route::post('/register', [JWTController::class, 'register']);
Route::post('/login', [JWTController::class, 'login'])->name('login');
